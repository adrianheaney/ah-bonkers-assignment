class AddDescriptionToSubReddit < ActiveRecord::Migration[5.1]
    def up
      add_column("sub_reddits", "description", :string, :limit => 255)
    end

    def down
      remove_column("sub_reddits", "description")
    end
end
