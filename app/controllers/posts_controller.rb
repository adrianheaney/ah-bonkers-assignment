require 'screencap'

class PostsController < ApplicationController
    def show
        @post = Post.find_by_id(params[:id])
        @comments = @post.comments.where(:parent_id => nil)
    end

    def new
        @sub_reddit = SubReddit.find_by_id(params[:sub_reddit_id])
        @post = Post.new()
    end

    def create
        @sub_reddit = SubReddit.find_by_id(params[:sub_reddit_id])
        @post = @sub_reddit.posts.create(post_params)

        if @post.save
            flash[:notice] = "Post added"
            redirect_to sub_reddit_post_path(@sub_reddit, @post)
        else
            flash[:error] = "Error creating comment"
            redirect_to new_sub_reddit_post(@sub_reddit, @post)
        end
    end

    private

    def post_params
        params.require(:post).permit(:title, :link, :description)
    end
end
