class SubReddit < ApplicationRecord
    has_many :posts

    scope :sorted, lambda { order(:created_at => "DESC") }
end
