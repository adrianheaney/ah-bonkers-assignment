class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.integer :sub_reddit_id

      t.timestamps
    end
  end
end
