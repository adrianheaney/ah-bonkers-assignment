class SubRedditsController < ApplicationController
  def index
      @sub_reddits = SubReddit.sorted
  end

  def show
      @sub_reddit = SubReddit.find_by_id(params[:id])
  end

  def new
      @sub_reddit = SubReddit.new()
  end

  def create
      @sub_reddit = SubReddit.new(sub_reddit_params)

      if @sub_reddit.save
          flash[:notice] = "Sub Reddit added"
          redirect_to sub_reddit_path(@sub_reddit)
      else
          flash[:error] = "Error creating comment"
          redirect_to new_sub_reddit_path()
      end
  end

  private

  def sub_reddit_params
      params.require(:sub_reddit).permit(:title, :description)
  end
end
