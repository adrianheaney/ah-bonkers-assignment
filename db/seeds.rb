# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

topics = SubReddit.create([{
        title: "Rails dev",
        description: "All things Ruby on Rails"
    }, {
        title: "The Simpsons",
        description: "Everything related to The Simpsons"
    }
])

topics.each do |t|
    posts = t.posts.create([
        {
            title: "Sample Post 1",
            link: "http://www.google.ie",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel arcu cursus, ultricies urna et, porta nulla."
        },
        {
            title: "Sample Post 2",
            link: "http://www.netflix.com",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel arcu cursus, ultricies urna et, porta nulla."
        }])

    posts.each do |p|
        p.comments.create([{
            name: "Commenter 1",
            link: "http://www.google.ie",
            text: "Nam augue nunc, blandit et dui cursus, dapibus fermentum diam. Phasellus ac purus arcu.",
        },{
            name: "Commenter 2",
            link: "http://www.google.ie",
            text: "Nam augue nunc, blandit et dui cursus, dapibus fermentum diam. Phasellus ac purus arcu."
        }])
    end
end
