
$('.add-comment').click(function() {
    $trigger = $(this);
    $form = $trigger.next('.comment-form');

    if ($form.hasClass('in')) {
        $form.removeClass('in');
        $trigger.text($trigger.data('add-label'));
    } else {
        $form.addClass('in');
        $trigger.text('Cancel');
    }
});
