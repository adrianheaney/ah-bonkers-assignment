class AlterPostsAddDescription < ActiveRecord::Migration[5.1]
    def up
      add_column("posts", "description", :string, :limit => 255)
      add_column("posts", "link", :string, :limit => 255)
    end

    def down
      remove_column("posts", "link")
      remove_column("posts", "description")
    end
end
