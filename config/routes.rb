Rails.application.routes.draw do

  root 'sub_reddits#index'

  resources :sub_reddits do
      resources :posts do
          resources :comments
      end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
