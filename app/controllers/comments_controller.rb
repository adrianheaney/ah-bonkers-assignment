class CommentsController < ApplicationController

    def create
        @post = Post.find_by_id(params[:post_id])
        @comment = @post.comments.create(comment_params)

        if @comment.save
            flash[:notice] = "Comment added"
            redirect_to sub_reddit_post_path(@post.sub_reddit, @post)
        else
            flash[:error] = "Error creating comment"
            redirect_to sub_reddit_post_path(@post.sub_reddit, @post)
        end
    end

    private

    def comment_params
        params.require(:comment).permit(:parent_id, :text, :name, :link, :email)
    end
end
