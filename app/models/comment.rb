class Comment < ApplicationRecord
    belongs_to :parent, :class_name => "Comment", :required => false
    has_many :comments, :foreign_key => "parent_id"
    belongs_to :post
end
