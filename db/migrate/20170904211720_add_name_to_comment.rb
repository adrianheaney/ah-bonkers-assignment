class AddNameToComment < ActiveRecord::Migration[5.1]
    def up
      add_column("comments", "name", :string, :limit => 255)
      add_column("comments", "link", :string, :limit => 255)
      add_column("comments", "email", :string, :limit => 255)
    end

    def down
      remove_column("comments", "email")
      remove_column("comments", "link")
      remove_column("comments", "name")
    end
end
